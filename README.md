scrapAY24
=========
Es un repositorio de ejemplos "Selenium" sobre python.
Se utilizo para el desarollo y pruebas:

- python3
- Selenium ( https://www.selenium.dev )
- Mozilla Firefox 82.0 (64bits)
- Google Chrome ChromeDriver 86.0.4240.22 (64bits)

Dentro de las carpeta "drivers" estan los geckodrivers para Windows y GNU/Linux.
Corroborar las dependencias con los ejemplos dentro de "test", una vez que estos se ejecuten ya puedes ejecutar los otros.

Mozilla GeckoDriver
===================
Si clona el repositorio no es necesario descargarlo, salvo que requiera el de 32bits o que haya quedado obsoleto, descargarlo de https://github.com/mozilla/geckodriver/releases y reemplazarlo en la carpeta correspodiente dentro de "drivers".

Chrome chromeDriver
===================
Si clona el repositorio no es necesario descargarlo, salvo que requiera el de 32bits o que haya quedado obsoleto, descargarlo de https://chromedriver.chromium.org/downloads y reemplazarlo en la carpeta correspodiente dentro de "drivers".

Instalación de Selenium en Windows
==================================
pip install -U selenium

Instalación de Selenium en Linux
================================
sudo apt update
sudo apt install python3-selenium

Instalación adicionales
=======================
- pip install keyboard
- pip install mysql.connector

Documentación selenium
======================
https://www.selenium.dev/documentation/es/

Autor
=====
- Matias Baez
- @matt_profe
- mbcorp.matias@gmail.com
- http://www.mbcorp.com.ar