import sys
# importamos selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from time import sleep

# Busca el OS que tenemos
urlDriver = '../drivers/' + sys.platform

# Si tenemos Chrome lo usa, sino usa firefox
try:
    browser = webdriver.Chrome(executable_path=urlDriver + "/chrome/chromedriver")
    print(">>> Google Chrome.")
except BaseException:
    browser = webdriver.Firefox(executable_path=urlDriver + "/firefox/geckodriver")
    print(">>> Mozilla Firefox.")

# Aviso de que estamos corriendo
print(">>> Aguarde un momento.")

WebDriverWait(browser, 10)

# URL con estadisticas del mundo
url = "https://www.worldometers.info/es/"

# carga de la pagina
browser.get(url)
print("Abrimos la web de estadisticas del mundo.")
print("========================================.")

print(">>> En 10 segundos se despliegan los datos.")

# esperamos a que se termine de cargar
sleep(10)

while(True):
    # capturamos la clase que tiene la cantidad de humanos
    contador = browser.find_element(By.CLASS_NAME, "rts-counter").text
    print("Poblacion Mundial: " + contador)
