import sys
# importamos selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
import time

# logueo
print("DNI: ", end="")
dni = input()
print("Pass: ", end="")
pas = input()

# Busca el OS que tenemos
urlDriver = '../drivers/' + sys.platform

# Si tenemos Chrome lo usa, sino usa firefox
try:
    browser = webdriver.Chrome(executable_path=urlDriver + "/chrome/chromedriver")
    print(">>> Google Chrome.")
except BaseException:
    browser = webdriver.Firefox(executable_path=urlDriver + "/firefox/geckodriver")
    print(">>> Mozilla Firefox.")

# carga de la pagina
browser.get("https://bullmarketbrokers.com/")
print("Abrimos Bullmarket.")

# esperamos a que no se vea el aviso de cargando
WebDriverWait(browser, 30000).until_not(expected_conditions.visibility_of_element_located((By.ID, "loading")))

# clic en el botón ingresar
browser.find_element(By.LINK_TEXT, "Ingresar").click()
print("Clic en ingresar.")

# esperamos a que se vea el boton de login del formulario 
WebDriverWait(browser, 30000).until(expected_conditions.visibility_of_element_located((By.ID, "btn_login_ok")))
browser.find_element(By.ID, "txt_modal_login_idNumber").send_keys(dni)
browser.find_element(By.ID, "txt_modal_login_password").send_keys(pas)
print("Cargamos los datos en el formulario.")

# Clic en el botón de login
browser.find_element(By.ID, "btn_login_ok").click()
print("Click en login.")

# esperamos unos segundos a que se carguen los valores del resumen de cuenta
time.sleep(5)

# Capturamos el resumen de la cuenta
resumen = browser.find_element(By.CLASS_NAME, "money-resume")
print("=================== Resumen de cuenta :)")
print(resumen.text)

# cierra la sesion
browser.find_element(By.LINK_TEXT, "Salir").click()
print("Cierra sesión.")

# cerramos el browser
# browser.quit()
# browser.close()
