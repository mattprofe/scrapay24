import sys
# importamos selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep

# logueo
print("Usuario: ", end="")
user = input()
print("Pass: ", end="")
pwd = input()

# Busca el OS que tenemos
urlDriver = '../drivers/' + sys.platform

# Si tenemos Chrome lo usa, sino usa firefox
try:
    browser = webdriver.Chrome(executable_path=urlDriver + "/chrome/chromedriver")
    print(">>> Google Chrome.")
except BaseException:
    browser = webdriver.Firefox(executable_path=urlDriver + "/firefox/geckodriver")
    print(">>> Mozilla Firefox.")

# carga de la pagina
browser.get('http://www.mattprofe.com.ar/mipanel/login.php')
print("Página abierta")

# escribo en las cajas de texto
browser.find_element_by_id("txtUsuario").send_keys(user)
browser.find_element_by_id("txtPassword").send_keys(pwd)
print("Carga de cajas de texto")

sleep(3)

# enter sobre el boton de logueo
browser.find_element_by_name("boton").send_keys(Keys.RETURN)
print("Presiono enter sobre el boton")
