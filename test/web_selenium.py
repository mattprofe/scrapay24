import sys
# importamos selenium
from selenium import webdriver

# Busca el OS que tenemos
urlDriver = '../drivers/' + sys.platform

# Si tenemos Chrome lo usa, sino usa firefox
try:
    browser = webdriver.Chrome(executable_path=urlDriver + "/chrome/chromedriver")
    print(">>> Google Chrome.")
except BaseException:
    browser = webdriver.Firefox(executable_path=urlDriver + "/firefox/geckodriver")
    # carga de la pagina
    print(">>> Mozilla Firefox.")

browser.get('http://seleniumhq.org/')
