import sys
# importamos selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# Busca el OS que tenemos
urlDriver = '../drivers/' + sys.platform

# Si tenemos Chrome lo usa, sino usa firefox
try:
    browser = webdriver.Chrome(executable_path=urlDriver + "/chrome/chromedriver")
    print(">>> Google Chrome.")
except BaseException:
    browser = webdriver.Firefox(executable_path=urlDriver + "/firefox/geckodriver")
    # carga de la pagina
    print(">>> Mozilla Firefox.")

browser.get('http://seleniumhq.org/')
# Abre una pestaña en el navegador
browser.find_element_by_tag_name("body").send_keys(Keys.CONTROL + "t")
