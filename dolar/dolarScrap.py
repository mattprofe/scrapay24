import sys
import os
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By


# Limpia la consola dependiendo del OS
def clrScr():
    # si el OS es linux
    if sys.platform == "linux":
        os.system("clear")
    # quizas es Windows
    else:
        os.system("cls")

# hacemos una espera para darle tiempo a que se cargue el driver
# wait = WebDriverWait(browser, 10)

# Objeto dolar
class Dolar:

    # Atributos de Dolar
    blueVenta = 0
    blueCompra = 0
    oficialVenta = 0
    oficialCompra = 0
    fechaHora = 0

    # Selecciona el sistema operativo, driver y navegador adecuado
    def setup_method(self):

        # Busca el OS que tenemos
        urlDriver = '../drivers/' + sys.platform

        # Si tenemos Chrome lo usa, sino usa firefox
        try:
            self.driver = webdriver.Chrome(executable_path=urlDriver + "/chrome/chromedriver")
            print(">>> Google Chrome.")
        except BaseException:
            self.driver = webdriver.Firefox(executable_path=urlDriver + "/firefox/geckodriver")
            print(">>> Mozilla Firefox.")

    # Cierra el driver
    def teardown_method(self):
        self.driver.quit()

    # Carga la página y las variables con los datos que nos interesan
    def load_dolar(self):
        self.driver.get("https://www.dolarhoy.com/")
        # CompraOficial
        self.oficialCompra = self.driver.find_element(By.CSS_SELECTOR, ".row:nth-child(1) > .col-12:nth-child(1) .col-6:nth-child(1) > .price").text
        # VentaOficial
        self.oficialVenta = self.driver.find_element(By.CSS_SELECTOR, ".row:nth-child(1) > .col-12:nth-child(1) .col-6:nth-child(2) > .price").text
        # CompraBlue
        self.blueCompra = self.driver.find_element(By.CSS_SELECTOR, ".row:nth-child(1) > .col-12:nth-child(2) .col-6:nth-child(1) > .price").text
        # VentaBlue
        self.blueVenta = self.driver.find_element(By.CSS_SELECTOR, ".row:nth-child(1) > .col-12:nth-child(2) .col-6:nth-child(2) > .price").text
        self.fechaHora = self.driver.find_element(By.CSS_SELECTOR, ".row:nth-child(1) > .col-12:nth-child(2) .col-7").text

    # Imprime en pantalla los datos que capturamos
    def print_dolar(self):
        clrScr()
        print(">>>> Dolar Argentina: https://www.dolarhoy.com/")
        print("====== Oficial")
        print("Compra: " + self.oficialCompra)
        print("Venta: " + self.oficialVenta)
        print("====== Blue")
        print("Compra: " + self.blueCompra)
        print("Venta: " + self.blueVenta)
        print(">>>> " + self.fechaHora)
        print("-->> Refresca cada 5 segundos <<--")


# Sección de ejecución
dolar = Dolar()

dolar.setup_method()

while(1):
    dolar.load_dolar()
    dolar.print_dolar()
    sleep(5)

dolar.teardown_method()
