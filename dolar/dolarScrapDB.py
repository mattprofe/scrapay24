import sys
import os
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
import mysql.connector

# Limpia la consola dependiendo del OS
def clrScr():
    # si el OS es linux
    if sys.platform == "linux":
        os.system("clear")
    # quizas es Windows
    else:
        os.system("cls")

# hacemos una espera para darle tiempo a que se cargue el driver
# wait = WebDriverWait(browser, 10)

# Objeto dolar
class Dolar:

    # Atributos de Dolar
    blueVenta = 0
    blueCompra = 0
    oficialVenta = 0
    oficialCompra = 0
    fechaHora = 0

    # Selecciona el sistema operativo, driver y navegador adecuado
    def setup_method(self):

        # Busca el OS que tenemos
        urlDriver = '../drivers/' + sys.platform

        # Si tenemos Chrome lo usa, sino usa firefox
        try:
            self.driver = webdriver.Chrome(executable_path=urlDriver + "/chrome/chromedriver")
            print(">>> Google Chrome.")
        except BaseException:
            self.driver = webdriver.Firefox(executable_path=urlDriver + "/firefox/geckodriver")
            print(">>> Mozilla Firefox.")

    # Cierra el driver
    def teardown_method(self):
        self.driver.quit()

    # Carga la página y las variables con los datos que nos interesan
    def load_dolar(self):
        self.driver.get("https://www.dolarhoy.com/")
        # CompraOficial
        self.oficialCompra = self.driver.find_element(By.CSS_SELECTOR, ".row:nth-child(1) > .col-12:nth-child(1) .col-6:nth-child(1) > .price").text.replace('$ ', '').replace(',', '.')
        # VentaOficial
        self.oficialVenta = self.driver.find_element(By.CSS_SELECTOR, ".row:nth-child(1) > .col-12:nth-child(1) .col-6:nth-child(2) > .price").text.replace('$ ', '').replace(',', '.')
        # CompraBlue
        self.blueCompra = self.driver.find_element(By.CSS_SELECTOR, ".row:nth-child(1) > .col-12:nth-child(2) .col-6:nth-child(1) > .price").text.replace('$ ', '').replace(',', '.')
        # VentaBlue
        self.blueVenta = self.driver.find_element(By.CSS_SELECTOR, ".row:nth-child(1) > .col-12:nth-child(2) .col-6:nth-child(2) > .price").text.replace('$ ', '').replace(',', '.')
        self.fechaHora = self.driver.find_element(By.CSS_SELECTOR, ".row:nth-child(1) > .col-12:nth-child(2) .col-7").text

    # Imprime en pantalla los datos que capturamos
    def print_dolar(self):
        clrScr()
        print(">>>> Dolar Argentina: https://www.dolarhoy.com/")
        print("====== Oficial")
        print("Compra: " + self.oficialCompra)
        print("Venta: " + self.oficialVenta)
        print("====== Blue")
        print("Compra: " + self.blueCompra)
        print("Venta: " + self.blueVenta)
        print(">>>> " + self.fechaHora)
        print("-->> Refresca cada 5 segundos <<--")


# Sección de ejecución

dolar = Dolar()

print("Host: ", end="")
host = input()
print("Usuario: ", end="")
user = input()
print("Password: ", end="")
pwd = input()
print("DB: ", end="")
db = input()
print("Existe la tabla (1/0): ", end="")
tabla = input()

# Bandera para cargar un registro solo si cambio el precio del dolar en la web
fechaHoraAnterior = 0

# Conección con la base de datos
mydb = mysql.connector.connect(host=host, user=user, password=pwd, database=db)
mycr = mydb.cursor()

# si no existe la tabla
if(tabla == "0"):
    # creamos la tabla
    ssql = "CREATE TABLE `" + db  + "`.`dolar_tracking` ( `idDolar` INT NOT NULL AUTO_INCREMENT , `oficialCompra` DOUBLE NOT NULL , `oficialVenta` DOUBLE NOT NULL , `blueCompra` DOUBLE NOT NULL , `blueVenta` DOUBLE NOT NULL , `fechaHora` TIMESTAMP NOT NULL , PRIMARY KEY (`idDolar`)) ENGINE = InnoDB;"

    mycr.execute(ssql)
    mydb.commit()
    print("Se creo la tabla: dolar_tracking")
    sleep(5)

# Arranque del driver
dolar.setup_method()

# Bucle de captura
while(1):
    # Analisis de la página y carga de variables
    dolar.load_dolar()
    # Muestra los valores en pantalla (comentar para usar menos recursor)
    dolar.print_dolar()

    # Si la web actualizo sus datos (lo razonamos por la fechaHora que muestra)
    if(fechaHoraAnterior != dolar.fechaHora):
        # Grabamos los datos en la tabla
        ssql = "INSERT INTO `dolar_tracking` (`idDolar`, `oficialCompra`, `oficialVenta`, `blueCompra`, `blueVenta`, `fechaHora`) VALUES (NULL, '" + str(dolar.oficialCompra)  + "', '" + str(dolar.oficialVenta)  + "', '" + str(dolar.blueCompra)  + "', '" + str(dolar.blueVenta) + "', CURRENT_TIMESTAMP);"
    
        mycr.execute(ssql)
        mydb.commit()
        # Preparamos la bandera
        fechaHoraAnterior = dolar.fechaHora
        print("==>> Datos Cargados en la DB")
    else:
        print("<<== Sin cambios, no se grabo en la DB")

    # Retardo para la recarga de la web
    sleep(5)

# Finaliza el browser
dolar.teardown_method()
