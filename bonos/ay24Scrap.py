import sys
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

# Busca el OS que tenemos
urlDriver = '../drivers/' + sys.platform

# Si tenemos Chrome lo usa, sino usa firefox
try:
    browser = webdriver.Chrome(executable_path=urlDriver + "/chrome/chromedriver")
    print(">>> Google Chrome.")
except BaseException:
    browser = webdriver.Firefox(executable_path=urlDriver + "/firefox/geckodriver")
    print(">>> Mozilla Firefox.")

while(1):

    # hacemos una espera para darle tiempo a que se cargue el driver
    wait = WebDriverWait(browser, 10)

    # carga de la pagina
    browser.get("https://www.puentenet.com/cotizaciones/bono/AY24D")

    # busqueda de la clase dentro de un span
    span = browser.find_element_by_css_selector('span.precio-panel-cotizacion')

    print("=========== AY24D")
    # for items in span:
    #   r = items.getText()
    #   g = r.split()
    #   var_dump(g)

    r = span.text
    g = r.split()
    print(g[1])
    # reemplazo , por .
    ay24d = float(g[1].replace(',', '.'))

    # ssssssssssssssssssssss

    # carga de la pagina
    browser.get("https://www.puentenet.com/cotizaciones/bono/AY24")

    # busqueda de la clase dentro de un span
    span = browser.find_element_by_css_selector('span.precio-panel-cotizacion')

    print("=========== AY24")
    # for items in span:
    #   r = items.getText()
    #   g = r.split()
    #   var_dump(g)

    r = span.text
    g = r.split()
    print(g[1])
    # reemplazo . por nada
    a = g[1].replace('.', '')
    # reemplazo , por .
    ay24 = float(a.replace(',', '.'))

    # calculo dolar bolsa en base al bono
    dolar_bolsa = round(ay24 / ay24d, 2)

    print("========================")
    print("Dolar Bolsa : " + str(dolar_bolsa))
    print("========================")

    # ssssssssssssssssssssss
browser.close()
