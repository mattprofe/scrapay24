import sys
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

# Busca el OS que tenemos
urlDriver = '../drivers/' + sys.platform

# Si tenemos Chrome lo usa, sino usa firefox
try:
    browser = webdriver.Chrome(executable_path=urlDriver + "/chrome/chromedriver")
    print(">>> Google Chrome.")
except BaseException:
    browser = webdriver.Firefox(executable_path=urlDriver + "/firefox/geckodriver")
    print(">>> Mozilla Firefox.")

# hacemos una espera para darle tiempo a que se cargue el driver
wait = WebDriverWait(browser, 10)

# abrimos la url
browser.get('https://www.clima.com/argentina/buenos-aires/escobar/por-horas')

# buscamos los datos de de un span con la clase especifica
temp = browser.find_element_by_css_selector('span.c-tib-text')

unit = browser.find_element_by_css_selector('sup.c-tib-unit')

# hume = browser.find_element_by_css_selector('div.m_table_weather_hour_detail_child_mobile m_table_weather_hour_detail_hum')


# mostramos el contenido del span
print(temp.text + unit.text)
# print(hume.text)

# cierro el navegador
browser.close()
