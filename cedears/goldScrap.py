import os
import sys
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import time


def tearDown(self):
    self.driver.close()


def main():
    while(1):
        # carga de la pagina
        browser.get("https://www.puentenet.com/cotizaciones/accion/GOLDCEDEAR")

        # busqueda de clases
        span = browser.find_element_by_css_selector('span.precio-panel-cotizacion')
        t = browser.find_element_by_css_selector('span.datoCotizacion')

        r = span.text
        g = r.split()

        # reemplazo de . por nada y , por .
        gold = float(g[1].replace('.', '').replace(',', '.'))
        print(str(gold) + " " + t.text)

        # hacemos una espera de 1 min para volver a analizar
        time.sleep(60)


if __name__ == '__main__':
    try:
        # Busca el OS que tenemos
        urlDriver = '../drivers/' + sys.platform

        # Si tenemos Chrome lo usa, sino usa firefox
        try:
            browser = webdriver.Chrome(executable_path=urlDriver + "/chrome/chromedriver")
            print(">>> Google Chrome.")
        except BaseException:
            browser = webdriver.Firefox(executable_path=urlDriver + "/firefox/geckodriver")
            print(">>> Mozilla Firefox.")

        # hacemos una espera para darle tiempo a que se cargue el driver
        wait = WebDriverWait(browser, 10)

        print("=========== Monitoreo GOLD")

        main()
    except KeyboardInterrupt:
        print('Finalizado.')
        browser.quit()
        browser.close()

    try:
        sys.exit(0)
    except SystemExit:
        os._exit(0)
